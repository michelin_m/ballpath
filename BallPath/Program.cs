﻿using System;
using System.Collections.Generic;
using System.IO;

namespace BallPath
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int n;
            int m;

            int i_start , j_start;

            int i_end, j_end;

            int[][] array2 = FileRead(out n, out m, out i_start, out i_end, out j_start, out j_end);
            
            char[] moves_arr = new char[m * n];

            array2[i_start][j_start] = 1;
            array2[i_end][j_end] = 1;

            int i_counter = -1;

            List<char> minPath= new List<char>(n*m);

            Step(i_start, j_start, i_end, j_end, n, m, array2, moves_arr,ref i_counter, ref minPath);

            Display(minPath, n, m, array2, i_start, i_end, j_start, j_end);
        }



        public static int[][] FileRead(out int n,out int m, out int i_s, out int i_e, out int j_s, out int j_e)
        {
            StreamReader sr = new StreamReader("/Users/mishamakh/Desktop/txt_tests/filetoread.txt");
            n = int.Parse(sr.ReadLine());
            m = int.Parse(sr.ReadLine());

            string buf = sr.ReadLine();
            string[] startPointStr = buf.Split(new char[] { ' ', ')', '(', ',' }, StringSplitOptions.RemoveEmptyEntries);

            buf = sr.ReadLine();
            string[] endPointStr = buf.Split(new char[] { ' ', ')', '(', ',' }, StringSplitOptions.RemoveEmptyEntries);

            i_s = int.Parse(startPointStr[0]);
            j_s = int.Parse(startPointStr[1]);

            i_e = int.Parse(endPointStr[0]);
            j_e = int.Parse(endPointStr[1]);

            string[] playFieldStr = sr.ReadToEnd().Replace(Environment.NewLine, " ").Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            sr.Close();

            int[][] array2 = new int[m][];

            for (int i = 0; i < m; i++)
            {
                array2[i] = new int[n];
                for (int j = 0; j < n; j++)
                {
                    array2[i][j] = int.Parse(playFieldStr[n * i + j]);
                }
            }

            return array2;
        }



        public static void Display(List<char> minPath, int n, int m, int[][] array2, int i_start, int i_end, int j_start, int j_end)
        {
            foreach (var item in minPath)
            {
                Console.Write($"{item} ");
            }

            Console.WriteLine("\n");

            char[][] array3 = new char[m][];

            for (int i = 0; i < m; i++)
            {
                array3[i] = new char[n];
                for (int j = 0; j < n; j++)
                {

                    if (i == i_end && j == j_end)
                        array3[i][j] = 'F';
                    else if (i == i_start && j == j_start)
                        array3[i][j] = 'S';
                    else if (array2[i][j] == 0)
                        array3[i][j] = 'O';
                    else
                        array3[i][j] = '1';

                }

            }

            if(minPath.Count == 0)
            {
                Console.WriteLine("There is no path");
            }

            Console.WriteLine(minPath.Count + "\n");

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write($"{array3[i][j]}  ");
                }
                Console.WriteLine("\n");
            }
        }

        public static void Step(int i_start, int j_start, int i_end, int j_end, int n, int m, int[][] array, char[] moves_arr, ref int i_counter, ref List<char> minPath)
        {
            i_counter++;

            if (i_start == i_end && j_start == j_end)
            { 
                if(minPath.Capacity>MinPath(moves_arr).Count)
                    minPath = new List<char>(MinPath(moves_arr));
                array[i_start][j_start] = 0;
            }

            if (array[i_start][j_start] != 0)
            {
                array[i_start][j_start] = 0;
                if (i_start < m-1)
                { 
                    moves_arr[i_counter] = 'D';
                    Step(++i_start, j_start, i_end, j_end, n, m, array, moves_arr, ref i_counter, ref minPath);// Down
                    --i_start;
                }
                if (j_start < n-1)
                { 
                    moves_arr[i_counter] = 'R';
                    Step(i_start, ++j_start, i_end, j_end, n, m, array, moves_arr, ref i_counter, ref minPath);// Right
                    --j_start;
                }
                if (i_start > 0)
                {
                    moves_arr[i_counter] = 'U';
                    Step(--i_start, j_start, i_end, j_end, n, m, array, moves_arr, ref i_counter, ref minPath);// Up
                    ++i_start;
                }
                if (j_start > 0)
                {
                    moves_arr[i_counter] = 'L';
                    Step(i_start, --j_start, i_end, j_end, n, m, array, moves_arr, ref i_counter, ref minPath);// Left
                    ++j_start;
                    
                }
                array[i_start][j_start] = 1;

            }
            if(i_counter>0)
                moves_arr[--i_counter] ='0';
            else
            {
                moves_arr[i_counter] = '0';
                i_counter--;
            }
            
        }
        public static List<char> MinPath(char[] arr)
        {
            List<char> Dynamic_size = new List<char>();
            foreach (var item in arr)
            {
                if (!Char.IsLetter(item))
                    break;
                Dynamic_size.Add(item);
            }
            return Dynamic_size;
        }
    }
}
